package com.jamesdube.notesgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotesGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotesGradleApplication.class, args);
	}
}
